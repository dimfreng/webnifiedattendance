class Attend < ActiveRecord::Base
	belongs_to :employee
	has_many :time_records, -> { order(:updated_at => :desc) }
	belongs_to :connection
	has_one :report
end
