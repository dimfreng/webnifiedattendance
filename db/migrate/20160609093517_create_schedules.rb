class CreateSchedules < ActiveRecord::Migration
  def up
    create_table :schedules do |t|
      t.column :monday_start ,:string
      t.column :tuesday_start, :string
      t.column :wednesday_start, :string
      t.column :thursday_start, :string
      t.column :friday_start, :string
      t.column :saturday_start, :string
      t.column :sunday_start, :string
      t.column :monday_end ,:string
      t.column :tuesday_end, :string
      t.column :wednesday_end, :string
      t.column :thursday_end, :string
      t.column :friday_end, :string
      t.column :saturday_end, :string
      t.column :sunday_end, :string
      t.column :connection_id, :integer
      t.index :connection_id
      t.timestamps null: false
    end
  end

  def down
  	drop_table :schedules
  end
end
