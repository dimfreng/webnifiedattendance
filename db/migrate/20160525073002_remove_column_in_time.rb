class RemoveColumnInTime < ActiveRecord::Migration
  def up
  	remove_column :time_records ,:commentIn
  	remove_column :time_records ,:commentOut
  	remove_column :time_records ,:timeOut
  	remove_column :time_records ,:emotion
  	rename_column :time_records ,:timeIn ,:timeSpan
  	rename_column :time_records ,:photoIn ,:task
  end


  def down
  	add_column :time_records ,:commentIn  ,:string
  	add_column :time_records ,:commentOut ,:string
  	add_column :time_records ,:timeOut ,:string
  	add_column :time_records ,:emotion ,:string
  	rename_column :time_records  ,:timeSpan ,:timeIn
  	rename_column :time_records  ,:task ,:photoIn
  end
end
