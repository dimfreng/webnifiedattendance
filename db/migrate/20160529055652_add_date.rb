class AddDate < ActiveRecord::Migration
  def up
  	add_column :attends, :date, :string
  end

  def down
  	remove_column :attends, :date
  end
end
