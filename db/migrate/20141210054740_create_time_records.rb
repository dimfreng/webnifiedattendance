class CreateTimeRecords < ActiveRecord::Migration
 
  def up
    create_table :time_records do |t|
      t.column :employee_id ,:integer
      t.column :timeIn ,:string
      t.column :timeOut ,:string
      t.column :date ,:string
      t.column :commentIn ,:string
      t.column :commentOut ,:string
      t.column :photoIn ,:string
      t.column :emotion ,:string
      t.index :employee_id
      t.timestamps
    end
  end

  def down
  	  drop_table :time_records
  end

end
