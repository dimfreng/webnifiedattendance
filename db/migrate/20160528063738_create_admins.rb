class CreateAdmins < ActiveRecord::Migration
  def up
    create_table :admins do |t|
      t.column :firstname, :string
      t.column :lastname, :string
      t.column :email, :string
      t.column :password_digest,:string
      t.timestamps
    end
  end

  def down
  	drop_table :admins
  end
end
