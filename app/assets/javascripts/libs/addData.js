virtual
	.directive( "addData", [
		"Server",
		"Util",
		"$interval",
		function directive ( Server, Util , $interval ) {
			return {
				"restrict": "A",
				"scope": true,				
				"link": function onLink ( scope , element , attributeSet ) {
					scope.report = { };
					scope.employees = { };
					scope.clients = { };
					scope.connections = { };
				 	scope.record = { };
					scope.record_id = 0;
					scope.attends = { };
					scope.connection = { };
					scope.todolist = [];
					scope.sched = { };
					scope.message = { };
					scope.va = { };
					scope.client = { };
					scope.admn = { };
					scope.todo = { };
					scope.attend_enable = 'false';
					scope.todoId = "";
					scope.showme = 0;
					scope.showUs = 0;
					scope.showForm = 0
					scope.attend_id = 0;
					var y,t,seconds = 0,
						minutes = 0,
						hours = 0,
						second = 0,
						minute = 0,
						hour = 0;

				 	scope.sendMail = function ( ) {
				 		$('#spinner').show();
				 		Server.sendReply( scope.message )
							.then( function ( response ) {
								scope.message = { };
								$('#spinner').hide();
								Util.showAlert( "check.png", "Successfully Added", "Notification Successfully Sent!" ,"error");
							} );			
				 	};

				 	scope.addVA = function ( ){
				 		Server.addEmployee( scope.va )
							.then( function ( response ) {
								if (response.data.message == "Email Already Exist"){
									Util.showAlert( "error.png", "Failed", "Email Already Exist" ,"error");
								}else{
									scope.va = { };
									scope.showme = 0;
									scope.getAll();
								}
							} );	
				 	};

				 	scope.addClient = function ( ){
				 		Server.addClient( scope.client )
							.then( function ( response ) {
								if (response.data.message == "Email Already Exist"){
									Util.showAlert( "error.png", "Failed", "Email Already Exist" ,"error");
								}else{
									scope.client = { };
									scope.showme = 0;
									scope.getAll();
								}
							} );	
				 	};

				 	scope.addAdmin = function ( ){
				 		Server.addAdmin( scope.admn )
							.then( function ( response ) {
								scope.admn = { };
								scope.showme = 0;
							} );	
				 	};
				 	scope.addConnection = function ( ){
				 		Server.addConnection( scope.connection )
							.then( function ( response ) {
								scope.connection = { };
								scope.showme = 0;
								scope.getAll();
							} );	
				 	};

				 	scope.addTodo = function ( type ){
				 		scope.todo.connection_id = scope.todoId;
				 		Server.addTodo( scope.todo )
							.then( function ( response ) {
								if (type == 'client'){
									scope.message.email = response.data.employee_email;
									scope.message.from = response.data.client_email;
									scope.message.task = response.data.todo.task;
									scope.message.deadline = response.data.todo.deadline;
									scope.sendMail();
								}else{
									scope.message.email = response.data.client_email;
									scope.message.from = response.data.employee_email;
									scope.message.task = response.data.todo.task;
									scope.message.deadline = response.data.todo.deadline;
									scope.sendMail();
								}
								
								scope.todo = { };
								scope.getTodos();
							} );	
				 	};


				 	scope.getAll = function( ){
				 		Server.getAll()
				 		.then( function( response ){
				 			scope.employees = response.data.employees;
				 			scope.clients = response.data.clients;
				 			scope.connections = response.data.connections;
				 			console.log(scope.connections);
				 		});
				 	};

				 	scope.addReport = function( ){
				 		scope.report.attend_id = scope.attend_id;
				 		Server.addReport( scope.report )
				 		.then( function( response ){
				 			Util.showAlert( "check.png", "Add Report", "Successfully Save" ,"error");
				 			scope.showme = 0;
				 			scope.report = { };
				 		});
				 	};

				 	scope.updateTask = function( id, status ){
				 			Server.updateTodo( id, status )
							.then( function ( response ) {
								if (status == "to Start" || status == "doing"){
									localStorage.setItem('connection_id',scope.todoId);
									localStorage.setItem('todo_id',response.data.todo.id);
								    localStorage.setItem('task',response.data.todo.task);
						            scope.getTodos(scope.todoId);
								    if ( status == "doing" ){
								    	Util.showAlert( "check.png", "Doing Task", "We are now recording you time for this Task" ,"error");
								    }else{
								    	Util.showAlert( "check.png", "Starting Task", "Please Proceed to Timer to Start Task Timer" ,"error");
								    }
								    
								}else if(status == "done") {
									localStorage.setItem('todo_id','');
								    localStorage.setItem('task','');
								    Util.showAlert( "check.png", "Done Task", "Find another task to start" ,"error");
								}else{
						 			scope.getTodos(scope.todoId);
									localStorage.setItem('todo_id','');
								    localStorage.setItem('task','');
								    if ( !localStorage.getItem('attendId') ){
								    	localStorage.setItem('connection_id', '');
								    	scope.attend_enable = "false";
								    }
								    Util.showAlert( "check.png", "Cancelled Task", "Find another task to start" ,"error");
								}
							} );
				 	};
					

					scope.setshowme = function ( data ){
						scope.showme = data;
					};

					scope.setshowUs = function ( data ){
						scope.showUs = data;
					};

					scope.closeshowme = function (  ){
						
						scope.showme = 0;
					};

					scope.editMe = function( number, id, type ){
						scope.showme = number;
						Server.getData( id, type  ).then( function ( response ) { 
							if(type == 'employee'){
								scope.va = response.data.data;
							}else{
								scope.client = response.data.data;
							}
						} );
					};

					scope.updateData = function( type, id ){
						scope.showme = 0;
						if (type == 'employee'){
							Server.updateData( id, type, scope.va  ).then( function ( response ) { 
									scope.va = response.data.data;
									window.location.href = "/info";
							} );
						}else{
							Server.updateData( id, type, scope.client  ).then( function ( response ) { 
									scope.client = response.data.data;
									window.location.href = "/client/info";
							} );
						}
					};

					scope.start = function( id ){
						Server.start( id, scope.attend_id  ).then( function ( response ) { 
							scope.record_id = response.data.record.id ;
							localStorage.setItem('task',scope.record.task);
							localStorage.setItem('recordId',scope.record_id);
							scope.showForm = 2;
							timer();
							 Util.showAlert( "check.png", "record now" ,"we are now recording your time for this task");
							 if (localStorage.getItem('todo_id')){
							 	scope.updateTask(localStorage.getItem('todo_id'), "doing");
							 }
						} );
					};


					scope.stop = function( ){
						scope.record.time_span = scope.hours+":"+scope.minutes+":"+scope.seconds;
						$interval.cancel(t);
						Server.stop( scope.record_id , scope.record ).then( function ( response ) { 
								 scope.showForm = 1;
								 localStorage.setItem('task','');
								 scope.hours = "00";
								 scope.minutes = "00";
								 scope.seconds = "00";
							     Util.showAlert( "check.png", "Add Task", "Successfully Added" ,"error");
							     localStorage.setItem('recordId','');
								 localStorage.setItem('seconds','');
						         localStorage.setItem('minutes','');
						         localStorage.setItem('hours','');
						         seconds = 0; minutes = 0; hours = 0;
								 scope.record.task = "";
								 if (localStorage.getItem('todo_id')){
								 	scope.updateTask(localStorage.getItem('todo_id'), "done");
								 }
								 
						} );
						
					};

					scope.getSched = function( id ){
						Server.getSched( id ).then( function ( response ) { 
							scope.sched = response.data.sched;
							localStorage.setItem('mysched', id);
						} );
					};

					scope.updateSched = function( ){
						scope.sched.connection_id = localStorage.getItem('connect');
						Server.updateSched( scope.sched ).then( function ( response ) { 
							scope.sched = response.data.sched;
							scope.getSched( scope.sched.connection_id );
							 Util.showAlert( "check.png", "Update Schedule" ,"Schedule has been updated");
						} );
					};

					scope.attend = function( id){
						Server.attend( id ,localStorage.getItem('connection_id') ).then( function ( response ) { 
							scope.attend_id = response.data.attend.id ;
							localStorage.setItem('attendId',scope.attend_id);
								 Util.showAlert( "check.png", "record now" ,"we are now recording your Time In / Time Out for your selected Client, Do more task for this client.");
								 scope.showForm = 1; 
								 timers();
						} );
					};


					scope.out = function( ){

						localStorage.setItem('connection_id','');
						scope.attend_enable = 'false';
						scope.attends.time_span = scope.hour+":"+scope.minute+":"+scope.second;
						$interval.cancel(y);
						Server.out( scope.attend_id , scope.attends ).then( function ( response ) { 
								scope.showForm = 0; 
								scope.hour = "00";
								scope.minute = "00";
								scope.second = "00";
								localStorage.setItem('attendId','');
								localStorage.setItem('second','');
						        localStorage.setItem('minute','');
						        localStorage.setItem('hour','');
						        scope.showme = 4;
						        y;
						        second = 0; minute = 0; hour = 0;
						} );
						
					};

					scope.checkme = function ( ){
						scope.getAll();
						scope.getSched(localStorage.getItem('mysched'));
						scope.todoId = localStorage.getItem('connect');
						scope.getTodos(scope.todoId);
						scope.record.task = localStorage.getItem('task');
						if( localStorage.getItem('connection_id') ){
							scope.attend_enable = "true";
							scope.todoId = localStorage.getItem('connection_id');
							scope.getTodos(scope.todoId);
						}
						if( localStorage.getItem('attendId') ){
							scope.attend_id =  localStorage.getItem('attendId');
							hour =   localStorage.getItem('hour');
							minute = localStorage.getItem('minute');
							second = localStorage.getItem('second');
							
							scope.showForm = 1; 
							timers();

							if( localStorage.getItem('recordId') ){
								scope.record_id = localStorage.getItem('recordId');
								scope.record.task = localStorage.getItem('task');
								hours =   localStorage.getItem('hours');
								minutes = localStorage.getItem('minutes');
								seconds = localStorage.getItem('seconds');
								
								scope.showForm = 2; 
								timer();
							}
						}else{
							scope.hours = "00";
							scope.minutes = "00";
							scope.seconds = "00";
							scope.hour = "00";
							scope.minute = "00";
							scope.second = "00";

						}
					};


					scope.getTodos = function ( ){
						localStorage.setItem('connect', scope.todoId);
						Server.getTodos( scope.todoId )
							.then( function ( response ) {
								scope.todolist = response.data.todos;
								scope.getSched(scope.todoId);
							} );	
					};

					
					scope.checkme();
					
					

					function add() {
					    seconds++;
					    if (seconds >= 60) {
					        seconds = 0;
					        minutes++;
					        if (minutes >= 60) {
					            minutes = 0;
					            hours++;
					        }
					    }

					    scope.hours = (hours ? (hours > 9 ? hours : "0" + hours) : "00");
					    scope.minutes = (minutes ? (minutes > 9 ? minutes : "0" + minutes) : "00");
					    scope.seconds =  (seconds > 9 ? seconds : "0" + seconds);
					    localStorage.setItem('seconds',seconds);
					    localStorage.setItem('minutes',minutes);
					    localStorage.setItem('hours',hours);
					    
					}

					function adds() {
					    second++;
					    if (second >= 60) {
					        second = 0;
					        minute++;
					        if (minute >= 60) {
					            minute = 0;
					            hour++;
					        }
					    }
					    scope.hour = (hour ? (hour > 9 ? hour : "0" + hour) : "00");
					    scope.minute = (minute ? (minute > 9 ? minute : "0" + minute) : "00");
					    scope.second =  (second > 9 ? second : "0" + second);
					    localStorage.setItem('second',second);
					    localStorage.setItem('minute',minute);
					    localStorage.setItem('hour',hour);
					}

					function timer() {
					    t = $interval(add, 1000);
					}

					function timers() {
					    y = $interval(adds, 1000);
					}


				}
			}
		}
	] );