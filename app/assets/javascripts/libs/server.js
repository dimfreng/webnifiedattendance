virtual
	.factory( "Server" , [
		"$http",
		function factory ( $http ) {
			var server = {
				"request": function restApi ( method , path , params ) {
					return $http[ method ]( path , ( params || { } ) );
				},
				"addEmployee": function addEmployee ( employee ) {
					return server.request( "post", "/api/add", {"employee" : employee} );
				}, 
				"addClient": function addClient ( client ) {
					return server.request( "post", "/api/add/client", {"client" : client} );
				},
				"addConnection": function addConnection ( connection ) {
					return server.request( "post", "/api/add/connection", {"connection" : connection} );
				}, 
				"addTodo": function addTodo ( todo ) {
					return server.request( "post", "/api/add/todo", {"todo" : todo} );
				},
				"getTodos": function getTodos ( id ) {
					return server.request( "post", "/api/get/todos", {"id" : id} );
				},
				"getSched": function getSched ( id ) {
					return server.request( "post", "/api/get/sched", {"id" : id} );
				},
				"getData": function getData ( id, type ) {
					return server.request( "post", "/api/get/data", {"id" : id, "type" : type} );
				},
				"getAll": function getAll ( ) {
					return server.request( "get", "/api/get/all");
				},
				"updateData": function updateData ( id, type, data ) {
					return server.request( "post", "/api/update/data", {"id" : id, "type" : type, "employee": data, "client": data} );
				},
				"updateSched": function updateSched ( sched ) {
					return server.request( "post", "/api/update/sched", { "schedule": sched} );
				},
				"updateTodo": function updateTodo ( id, status ) {
					return server.request( "post", "/api/update/todo", {"id" : id, "status": status} );
				},
				"addAdmin": function addAdmin ( admin ) {
					return server.request( "post", "/api/add/admin", {"admin" : admin} );
				},
				"start": function start ( employee_id, attend_id ) {
					return server.request( "post", "/api/record/start", {"employee_id" : employee_id, "attend_id" : attend_id} );
				},
				"stop": function stop ( id, record ) {
					return server.request( "post", "/api/record/stop", {"id" : id, "record": record} );
				},
				"attend": function attend ( employee_id, connection_id) {
					return server.request( "post", "/api/attend/start", {"employee_id" : employee_id , "connection_id": connection_id} );
				},
				"out": function out ( id, attend ) {
					return server.request( "post", "/api/attend/stop", {"id" : id, "attend": attend} );
				},
				"sendReply": function sendReply ( message ) {
					return server.request( "post", "/api/send/reply", {"message" : message} );
				},
				"addReport": function addReport ( report ) {
					return server.request( "post", "/api/add/report", {"report" : report} );
				}
			};

			return server;
		}
	] );