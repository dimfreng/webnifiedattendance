class AddColumn < ActiveRecord::Migration
  def up
  	add_column :time_records, :connection_id, :integer
  	add_index :time_records, :connection_id
  end

  def down
  	remove_index :time_records, :connection_id
  	remove_column :time_records, :connection_id
  end
end
