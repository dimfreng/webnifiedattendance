class AddPassword < ActiveRecord::Migration
  def up
  	add_column :clients ,:password_digest,:string
  	add_column :employees ,:link_in, :text
  	add_column :employees ,:facebook, :text
  	add_column :employees ,:skill, :text
  	add_column :employees ,:profile_pic, :text
  end

  def down
  	remove_column :clients ,:password_digest
  	remove_column :employees ,:link_in
  	remove_column :employees ,:facebook
  	remove_column :employees ,:skill
  	remove_column :employees ,:profile_pic
  end
end
