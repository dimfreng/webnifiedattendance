class ApiController < ApplicationController

	def addEmployee
		if @employee = Employee.find_by_email(employee_params[:email])

			@message = "Email Already Exist"

		else
			if @employee = Employee.create(employee_params)
				@message = "succesfully created."
			else
				@message = "Failed to create Employee."
			end
		end

		render json:{message: @message, employee: @employee}
	end

	def addreport
		if @report = Report.create(report_params)
			@message = "succesfully created."
		else
			@message = "Failed to create Employee."
		end

		render json:{message: @message, report: @report}
	end

	def getAll
		@employees = Employee.all
		@clients = Client.all
		@connections = Connection.all.as_json(include: [{client: {except: :password_digest}}, {employee: {except: :password_digest}}]);

		render json:{ employees:@employees, clients:@clients, connections:@connections }
	end

	def send_reply
		message = params[:message]
		TrackerMailer.send_reply(message).deliver_now
		 render json: {:status => "success"}, status: :created
	end

	def getData 
		if params[:type] == 'employee'
			@data = Employee.find_by_id(params[:id])
		else
			@data = Client.find_by_id(params[:id])
		end

		render json:{data: @data}
	end

	def updateData 
		if params[:type] == 'employee'
			@data = Employee.find_by_id(params[:id])
			@data.update_attributes(employee_params)
		else
			@data = Client.find_by_id(params[:id])
			@data.update_attributes(client_params)
		end

		render json:{data: @data}
	end

	def updateSchedule
		if @sched = Connection.find_by_id(schedule_params[:connection_id]).schedule
			@sched.update_attributes(schedule_params)
			@message = "succesfully updated"
		 else
			@sched = Schedule.create(schedule_params)
			@message = "succesfully created"
		 end

		 render json:{message: @message, sched: @sched}
	end

	def getSchedule
		if @sched = Connection.find_by_id(params[:id]).schedule
			@message = "succesfully updated"
		 else
			@message = "failed"
		 end

		 render json:{message: @message, sched: @sched}
	end

	def addClient

		if @client = Client.find_by_email(client_params[:email])
			
			@message = "Email Already Exist"

		else
			if @client = Client.create(client_params)
				@message = "succesfully created."
			else
				@message = "Failed to create client."
			end
		end
		render json:{message: @message, client: @client}

	end

	def addTodo
		if @todo = Todo.create(todo_params)
			@message = "succesfully created."
			@employee_email = @todo.connection.employee.email
			@client_email = @todo.connection.client.email
		else
			@message = "Failed to create todo."
		end

		render json:{message: @message, todo: @todo, employee_email: @employee_email, client_email: @client_email}
	end

	def getTodos
		if @connection = Connection.find_by_id(params[:id])
			@todos = @connection.todos.order("deadline ASC")
			@message = "succesfully created."
		else
			@message = "Failed to create todo."
		end

		render json:{message: @message, todos: @todos}
	end

	def updateTodo
		if @todo = Todo.find_by_id(params[:id])
			@todo.status = params[:status]
			@todo.save
			@message = "succesfully created."
		else
			@message = "Failed to create todo."
		end

		render json:{message: @message, todo: @todo}
	end
	

	def addAdmin

		if @admin = Admin.create(admin_params)
			@message = "succesfully created."
		else
			@message = "Failed to create Employee."
		end

		render json:{message: @message, admin: @admin}

	end

	def addConnection
		if @connection = Connection.create(connection_params)
			@message = "succesfully created."
		else
			@message = "Failed to create connection."
		end

		render json:{message: @message, connection: @connection}
	end

	def attend
		
		@attend = Attend.new
		@attend.employee_id = params[:employee_id]
		@attend.connection_id = params[:connection_id]
		@attend.date = Time.now.strftime("%B-%d-%Y")
		@attend.save

		render json: {attend: @attend}
		
	end

	def out

		if attend = Attend.find_by_id(params[:id])
			if attend.update_attributes(attend_params)
				status = 400
				message = "succesfully update"
			else
				attend = nil
				message = "failed to udpate"
				status = 404
			end
		else
			status = 404
			message = "attend not found."
		end

		render json: {attend: attend, message: message}
		
	end

	
	def start
		
		@record = TimeRecord.new
		@record.attend_id = params[:attend_id]
		@record.employee_id = params[:employee_id]
		@record.date = Time.now.strftime("%B-%d-%Y")
		@record.save

		render json: {record: @record}
		
	end

	def stop

		if record = TimeRecord.find_by_id(params[:id])
			if record.update_attributes(record_params)
				status = 400
				message = "succesfully update"
			else
				record = nil
				message = "failed to udpate"
				status = 404
			end
		else
			status = 404
			message = "record not found."
		end

		render json: {record: record, message: message}
		
	end



	private

	def employee_params
		params.require(:employee).permit(:firstname, :lastname, :email,:link_in, :facebook, :skill, :password, :profile_pic)
	end

	def client_params
		params.require(:client).permit(:firstname, :lastname, :email,:time_zone, :website, :link_in, :password, :company, :subscription, :industry)
	end

	def admin_params
		params.require(:admin).permit(:firstname, :lastname, :email, :password)
	end

	def connection_params
		params.require(:connection).permit(:employee_id, :client_id)
	end

	def schedule_params
		params.require(:schedule).permit(:monday_start, :tuesday_start, :wednesday_start, :thursday_start, :friday_start, :saturday_start ,
			:sunday_start, :monday_end, :tuesday_end, :wednesday_end, :thursday_end, :friday_end, :saturday_end, :sunday_end, :connection_id)
	end

	def todo_params
		params.require(:todo).permit(:task, :deadline, :connection_id)
	end

	def record_params
		params.require(:record).permit(:task ,:time_span)
	end

	def attend_params
		params.require(:attend).permit(:time_span)
	end

	def report_params
		params.require(:report).permit(:roadblocks, :notes, :attend_id)
	end
end
