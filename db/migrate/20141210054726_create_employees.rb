class CreateEmployees < ActiveRecord::Migration
 
  def up
    create_table :employees do |t|
      t.column :firstname ,:string
      t.column :lastname ,:string
      t.column :position ,:string
      t.column :email ,:string
      t.column :contact ,:integer
      t.column :status ,:string
      t.column :profile ,:string
      t.column :password_digest,:string
      t.timestamps
    end
  end

  def down
  	 drop_table :employees
  end

end

