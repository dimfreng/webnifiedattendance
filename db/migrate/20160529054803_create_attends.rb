class CreateAttends < ActiveRecord::Migration
  def up
    create_table :attends do |t|
      t.column :employee_id, :integer
      t.column :timeSpan, :string
      t.index  :employee_id
      t.timestamps
    end
  end

  def down
  	drop_table :attends
  end
end
