class AddId < ActiveRecord::Migration
  def up
  	add_column :time_records, :attend_id, :integer
  	add_index :time_records, :attend_id
  end

  def down
  	remove_index :time_records, :attend_id
  	remove_column :time_records, :attend_id
  end
end
