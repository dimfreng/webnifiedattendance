class MainController < ApplicationController

  def profile
    if !session[:accout]
      redirect_to action: "login"
    else
      @employee = Employee.find_by_id(session[:user_id])
    end
  end

  def dtr
    if !session[:accout]
      redirect_to action: "login"
    end
  end

  def login
    if session[:accout]
      redirect_to action: "profile" 
    elsif session[:client_id]
      redirect_to action: "clientprofile"
    end
  end

  def myprofile

     if params[:id] == nil
      if (@employee = Employee.find_by_id(session[:user_id])) && session[:accout]
         @connections = @employee.connections.as_json(include: {client: {except: :password_digest}})
        @message = "success"
      else
        @message = "failed"
        redirect_to action: "login"
      end
    else
      if @employee = Employee.find_by_id(params[:id])
         @connections = @employee.connections.as_json(include: {client: {except: :password_digest}})
         @message = "success"
      else
        @message = "failed"
      end
    end
  end

  def clientprofile
    if params[:id] == nil
      if session[:client_id]
         if @client = Client.find_by_id(session[:client_id])
           @connections = @client.connections.as_json(include: {employee: {except: :password_digest}})
           @time_zones = ActiveSupport::TimeZone.all.map(&:name)
          @message = "success"
        else
          @message = "failed"
        end
      else
        redirect_to action: "login"
      end
     
    else
      if @client = Client.find_by_id(params[:id])
        @message = "success"
      else
        @message = "failed"
      end
    end
    
  end

  def logout
    session[:accout] = false
    session[:user_id] = nil
    session[:client_id] = nil
    session[:admin] = false
    redirect_to action: "login"
  end

  def adlogin
    if session[:admin]
      redirect_to action: "admin" 
    end
  end

  def check 
    if params[:kind] == "va" 
      if @employee = Employee.find_by_email(params[:email])
        if @employee.authenticate(params[:password])
          
            session[:accout] = true
            session[:user_id] = @employee.id
            redirect_to :action => "myprofile"
        
        else
          session[:accout] = false
          redirect_to :action => "login" 
        end
      else
        session[:accout] = false
        redirect_to :action => "login" 
      end
    else
     if @client = Client.find_by_email(params[:email])
        if @client.authenticate(params[:password])
          
            session[:client_id] = @client.id
            redirect_to :action => "clientprofile"
        
        else
          redirect_to :action => "login" 
        end
      else
        redirect_to :action => "login" 
      end
    end
      
  end

  def records
    if params[:id] != nil
      session[:connection] = params[:id]
      @connection = Connection.find_by_id(session[:connection])
      @records = @connection.attends.order('updated_at DESC').as_json(include: [:time_records, :report])
      @employee = @connection.employee
      
      @url = current_uri = request.env['PATH_INFO'] + ".pdf"
    else
      puts "***************this is it"
      @connection = Connection.find_by_id(session[:connection])
      @records = @connection.attends.order('updated_at DESC').as_json(include: [:time_records, :report])
       @employee = @connection.employee
      @url = current_uri = request.env['PATH_INFO'] + ".pdf"
    end
    
    #Prawn Start
    respond_to do |format|
      format.html
      format.pdf do
        pdf = ReportPdf.new(@records, @employee)
        send_data pdf.render, filename: 'attendanceReport.pdf', type: 'application/pdf', disposition: 'inline'
      end
    end
  end

  def viewInfo
    if @connection = Connection.find_by_id(params[:id])
       if params[:type] == "client"
          @data = @connection.client
          @type = "client"
       else
          @data = @connection.employee
          @type = "employee"
       end
    end
  end

  def checkAdmin
      
      if @admin = Admin.find_by_email(params[:email])
        if @admin.authenticate(params[:password])
          session[:admin] = true
          
          redirect_to :action => "admin"
        else
          session[:admin] = false
          redirect_to :action => "adlogin" 
        end
      else
        session[:admin] = false
        redirect_to :action => "adlogin" 
      end
  end

  def admin
    if !session[:admin]
      redirect_to action: "adlogin"
    else
      @employees = Employee.all.order("firstname ASC")
      @clients = Client.all.order("firstname ASC")
      @employee = Employee.new
      @client = Client.new
      @connections = Connection.all
      @time_zones = ActiveSupport::TimeZone.all.map(&:name)
      session[:id] = nil
    end
  end	

  def record
    if params[:id] != nil
      session[:connection] = params[:id]
      @connection = Connection.find_by_id(session[:connection])
      @records = @connection.attends.order('updated_at DESC').as_json(include: [:time_records, :report])
      @employee = @connection.employee
      
      @url = current_uri = request.env['PATH_INFO'] + ".pdf"
    else
      @connection = Connection.find_by_id(session[:connection])
      @records = @connection.attends.order('updated_at DESC').as_json(include: [:time_records, :report])
       @employee = @connection.employee
      @url = current_uri = request.env['PATH_INFO'] + ".pdf"
    end
    
    #Prawn Start
    respond_to do |format|
      format.html
      format.pdf do
        pdf = ReportPdf.new(@records, @employee)
        send_data pdf.render, filename: 'attendanceReport.pdf', type: 'application/pdf', disposition: 'inline'
      end
    end
  end



  def total_time ( id )

    records = Attend.find_by_id( id ).time_records

    hours   = 0
    minutes = 0
    seconds = 0

    records.each do |data|
      if data.time_span != nil
        a = data.time_span.split(":").map { |s| s.to_i }
      
        if (seconds += a[2]) >= 60
          minutes += (seconds / 60)
          seconds %= 60
            if (minutes += a[1]) >= 60
              hours += (minutes / 60)
              hours += a[0]
              minutes %= 60
            else
              hours += a[0]
            end
        else
          if (minutes += a[1]) >= 60
              hours += (minutes / 60)
              hours += a[0]
              minutes %= 60
            else
              hours += a[0]
            end
        end
      else
      end
    end
    return hours.to_s+":"+minutes.to_s+":"+seconds.to_s
  end

  helper_method :total_time

end
