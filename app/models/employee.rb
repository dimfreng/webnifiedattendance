class Employee < ActiveRecord::Base

	has_secure_password
	has_many :time_records
	has_many :connections
	has_many :attends
	has_attached_file :image
end
