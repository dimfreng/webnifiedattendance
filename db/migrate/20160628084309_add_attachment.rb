class AddAttachment < ActiveRecord::Migration
  def up
  	add_attachment :employees, :image
  	add_attachment :clients, :image
  end

  def down
  	remove_attachment :employees, :image
  	remove_attachment :clients, :image
  end
end
