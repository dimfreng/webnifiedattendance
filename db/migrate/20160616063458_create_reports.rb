class CreateReports < ActiveRecord::Migration
  def up
    create_table :reports do |t|
      t.column :attend_id, :integer
      t.column :roadblocks, :string
      t.column :notes, :string
      t.index :attend_id
      t.timestamps null: false
    end
  end

  def down
  	drop_table :reports
  end
end
