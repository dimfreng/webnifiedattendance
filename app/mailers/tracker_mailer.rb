class TrackerMailer < ApplicationMailer
	default from: 'virtualahan@gmail.com'
	
	 def send_reply(message)
	    @message = message
	    @email = @message[:email]
	    @url  = 'http://example.com/login'
	    mail(to: @email , subject: "You have an Email from Virtualahan")
	  end
end
