class CreateConnections < ActiveRecord::Migration
  def up
    create_table :connections do |t|
      t.column :employee_id, :integer
      t.column :client_id, :integer
      t.column :state, :text ,default: "active"
      t.index :employee_id
      t.index :client_id
      t.timestamps null: false
    end
  end

  def down
  	drop_table :connections
  end
end
