# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160628084309) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admins", force: :cascade do |t|
    t.string   "firstname",       limit: 255
    t.string   "lastname",        limit: 255
    t.string   "email",           limit: 255
    t.string   "password_digest", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "attends", force: :cascade do |t|
    t.integer  "employee_id"
    t.string   "time_span",     limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "date",          limit: 255
    t.integer  "connection_id"
  end

  add_index "attends", ["connection_id"], name: "index_attends_on_connection_id", using: :btree
  add_index "attends", ["employee_id"], name: "index_attends_on_employee_id", using: :btree

  create_table "clients", force: :cascade do |t|
    t.string   "firstname"
    t.string   "lastname"
    t.string   "email"
    t.text     "time_zone"
    t.text     "website"
    t.text     "link_in"
    t.text     "company"
    t.text     "subscription"
    t.text     "industry"
    t.text     "profile_pic"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "password_digest"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  create_table "connections", force: :cascade do |t|
    t.integer  "employee_id"
    t.integer  "client_id"
    t.text     "state",       default: "active"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  add_index "connections", ["client_id"], name: "index_connections_on_client_id", using: :btree
  add_index "connections", ["employee_id"], name: "index_connections_on_employee_id", using: :btree

  create_table "employees", force: :cascade do |t|
    t.string   "firstname"
    t.string   "lastname"
    t.string   "position"
    t.string   "email"
    t.string   "status"
    t.text     "profile"
    t.string   "password_digest"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "contact"
    t.text     "link_in"
    t.text     "facebook"
    t.text     "skill"
    t.text     "profile_pic"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  create_table "reports", force: :cascade do |t|
    t.integer  "attend_id"
    t.string   "roadblocks"
    t.string   "notes"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "reports", ["attend_id"], name: "index_reports_on_attend_id", using: :btree

  create_table "schedules", force: :cascade do |t|
    t.string   "monday_start"
    t.string   "tuesday_start"
    t.string   "wednesday_start"
    t.string   "thursday_start"
    t.string   "friday_start"
    t.string   "saturday_start"
    t.string   "sunday_start"
    t.string   "monday_end"
    t.string   "tuesday_end"
    t.string   "wednesday_end"
    t.string   "thursday_end"
    t.string   "friday_end"
    t.string   "saturday_end"
    t.string   "sunday_end"
    t.integer  "connection_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "schedules", ["connection_id"], name: "index_schedules_on_connection_id", using: :btree

  create_table "time_records", force: :cascade do |t|
    t.integer  "employee_id"
    t.string   "time_span"
    t.string   "date"
    t.text     "task"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "attend_id"
    t.integer  "connection_id"
  end

  add_index "time_records", ["attend_id"], name: "index_time_records_on_attend_id", using: :btree
  add_index "time_records", ["connection_id"], name: "index_time_records_on_connection_id", using: :btree
  add_index "time_records", ["employee_id"], name: "index_time_records_on_employee_id", using: :btree

  create_table "todos", force: :cascade do |t|
    t.text     "task"
    t.date     "deadline"
    t.string   "status",        default: "pending"
    t.integer  "connection_id"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  add_index "todos", ["connection_id"], name: "index_todos_on_connection_id", using: :btree

end
