class Change < ActiveRecord::Migration
  def up
  	rename_column :time_records ,:timeSpan ,:time_span
  	rename_column :attends ,:timeSpan ,:time_span
  end

  def down
  	rename_column :time_records  ,:time_span,:timeSpan
  	rename_column :attends  ,:time_span,:timeSpan
  end
end
