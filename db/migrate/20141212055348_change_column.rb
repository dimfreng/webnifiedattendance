class ChangeColumn < ActiveRecord::Migration
  def up
  	remove_column :employees, :contact
  	add_column :employees, :contact ,:string
  end

   def down
   	remove_column :employees, :contact
  	add_column :employees, :contact ,:integer
  end
end
