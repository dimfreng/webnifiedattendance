class Connection < ActiveRecord::Base
	belongs_to :client
	belongs_to :employee
	has_many :todos
	has_many :time_records
	has_many :attends
	has_one :schedule
end
