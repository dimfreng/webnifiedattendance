class CreateTodos < ActiveRecord::Migration
  def change
    create_table :todos do |t|
      t.column :task ,:text
      t.column :deadline ,:date
      t.column :status ,:string ,:default => "pending"
      t.column :connection_id ,:integer
      t.index :connection_id
      t.timestamps null: false
    end
  end
end
