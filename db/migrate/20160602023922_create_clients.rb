class CreateClients < ActiveRecord::Migration
  def up
    create_table :clients do |t|
      t.column :firstname, :string
      t.column :lastname, :string
      t.column :email, :string
      t.column :time_zone, :text
      t.column :website, :text
      t.column :link_in, :text
      t.column :company, :text
      t.column :subscription, :text
      t.column :industry, :text
      t.column :profile_pic, :text
      t.timestamps null: false
    end
  end


  def down
  	drop_table :clients
  end
end
