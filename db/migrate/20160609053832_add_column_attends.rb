class AddColumnAttends < ActiveRecord::Migration
 def up
  	add_column :attends, :connection_id, :integer
  	add_index :attends, :connection_id
  end

  def down
  	remove_index :attends, :connection_id
  	remove_column :attends, :connection_id
  end
end
