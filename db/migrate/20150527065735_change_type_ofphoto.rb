class ChangeTypeOfphoto < ActiveRecord::Migration
  def up
  	change_column :time_records, :photoIn, :text
  	change_column :employees, :profile ,:text
  end

  def down
  	change_column :time_records, :photoIn, :string
  	change_column :employees, :profile ,:string
  end
end
