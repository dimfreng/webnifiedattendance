	class ReportPdf < Prawn::Document

	def initialize(records, employee)
		super()
		@employee = employee
		@records = records
		# @total_time = total_time
		
		 line_item_rows
		
	end

	def total_time ( id )

    records = Attend.find_by_id( id ).time_records

    hours   = 0
    minutes = 0
    seconds = 0

    records.each do |data|
      if data.time_span != nil
        a = data.time_span.split(":").map { |s| s.to_i }
      
        if (seconds += a[2]) >= 60
          minutes += (seconds / 60)
          seconds %= 60
            if (minutes += a[1]) >= 60
              hours += (minutes / 60)
              hours += a[0]
              minutes %= 60
            else
              hours += a[0]
            end
        else
          if (minutes += a[1]) >= 60
              hours += (minutes / 60)
              hours += a[0]
              minutes %= 60
            else
              hours += a[0]
            end
        end
      else
      end
    end
    return hours.to_s+":"+minutes.to_s+":"+seconds.to_s
  end

	def line_item_rows

		image "#{Rails.root}/app/assets/images/virualahan.png", :width => 200
	   
	   move_down 20
	   text @employee.firstname.capitalize + " " + @employee.lastname.capitalize, :style => :bold
 		y_position = cursor - 50
 	   text "REPORT" , :style => :bold, :position => :center
 		y_position = cursor - 50
 		move_down 20
 		@records.each do |record| 
 		    data = [["Task","Total Hours","Start Time","End Time"]]
	  		data2 = [["Total Time", "Clock In", "Clock Out", "Date"]]
	  		data3 = [["RoadBlocks", "Notes/Questions/Reminders"]]
		
	  		data2 = data2 + [[record["time_span"], record["created_at"].strftime('%I:%M %P'), record["updated_at"].strftime('%I:%M %P'), record["date"]]]
			
			if record["report"] != nil
				data3 = data3 + [[record["report"]["roadblocks"], record["report"]["notes"]]]
			end

			record["time_records"].each do | time |
				data = data + [[time["task"], time["time_span"], time["created_at"].strftime('%I:%M %P'), time["updated_at"].strftime('%I:%M %P')]]
			end
			
			table(data2 ,:column_widths => [130,130,130,130]) do
	 			 position = :center
				 header = true
				 row(0).font_style = :bold
				 cells.padding_left = 5
				 cells.padding_right = 5
				 cells.padding_top = 7
				 cells.padding_bottom = 7
				 cells.borders = []
				 cells.borders = [:bottom]
				 cells.border_width = 1.5
				 cells.border_color = "7EC0EE"
			end
		
			table(data ,:column_widths => [130,130,130,130]) do
	 			 position = :center
				 header = true
				 row(0).font_style = :bold
				 cells.padding_left = 5
				 cells.padding_right = 5
				 cells.padding_top = 7
				 cells.padding_bottom = 7
				 cells.borders = []
				 cells.borders = [:bottom]
				 cells.border_width = 1.5
				 cells.border_color = "7EC0EE"
			end

			table(data3 ,:column_widths => [200,200]) do
	 			 position = :center
				 header = true
				 row(0).font_style = :bold
				 cells.padding_left = 5
				 cells.padding_right = 5
				 cells.padding_top = 7
				 cells.padding_bottom = 7
				 cells.borders = []
				 cells.borders = [:bottom]
				 cells.border_width = 1.5
				 cells.border_color = "7EC0EE"
			end
			move_down 20
			text "TOTAL RENDERED TIME:  " +self.total_time( record["id"] ) ,:style => :bold, :position => :center
			move_down 30
		end
			move_down 20

			
	 		
	end
 end